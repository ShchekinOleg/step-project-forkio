const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('delete');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

function buildCSS() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('dist'))
}

function buildHTML() {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('dist'))
}

function buildJS() {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(concat("script.js"))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('dist'))
}

function buildImages() {
    return gulp.src('src/img/*')
        .pipe(browserSync.stream())
        .pipe(gulp.dest('dist/img'))
}

function clean() {
    return del('dist/**', { force: true });
}

function build() {
    return gulp.series([
        clean,
        gulp.parallel([
            buildImages,
            buildCSS,
            buildHTML,
            buildJS
        ])
    ])
}

function start() {
    gulp.watch('src/**/*', build())
}

gulp.task('serve', function() {
    browserSync.init({
        server: { baseDir: 'dist' }
    })
    gulp.watch('src/*.html', gulp.series(['buildHTML']));
    gulp.watch('src/scss/*.scss', gulp.series(['buildCSS']));
    gulp.watch('src/js/*.js', gulp.series(['buildJS']));
    // gulp.watch('src/img/*', gulp.series(['buildImages']));
    gulp.watch('src/*.html').on('change', () => browserSync.reload())
});

exports.buildHTML = buildHTML;
exports.buildCSS = buildCSS;
exports.buildJS = buildJS;
exports.buildImages = buildImages;
exports.clean = clean;
exports.start = start;
exports.default = build();